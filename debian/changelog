faudio (20.04-2) unstable; urgency=medium

  * Eliminate cmake build percent output.

 -- Michael Gilbert <mgilbert@debian.org>  Sat, 04 Apr 2020 15:11:27 +0000

faudio (20.04-1) unstable; urgency=medium

  * New upstream release (closes: #955500).
  * Exclude cmake files from the binary packages.
  * Detect files missing from the binary packages.
  * Update standards version to 4.5.0 (no changes required).

 -- Michael Gilbert <mgilbert@debian.org>  Fri, 03 Apr 2020 01:46:34 +0000

faudio (19.12-1) unstable; urgency=medium

  * Fix debian/watch.
  * Drop obsolete lintian override 'shlib-calls-exit'.
  * Add README.source.
  * Add Vcs-* fields for new salsa.d.o git repository.
  * Refresh patches.

 -- Jens Reyer <jre.winesim@gmail.com>  Sun, 08 Dec 2019 17:00:40 +0100

faudio (19.11-1) unstable; urgency=medium

  * Update to debhelper 12.
  * Update standards version to 4.4.0.
  * Include Jens Reyer as an uploader.
  * New upstream release (closes: #940203).

 -- Michael Gilbert <mgilbert@debian.org>  Sat, 09 Nov 2019 02:47:48 +0000

faudio (19.07-1) unstable; urgency=medium

  * New upstream release.
  * Build with large file support.
  * Update standards version to 4.4.0.
  * Specify Build-Depends-Package in the symbols file.

 -- Michael Gilbert <mgilbert@debian.org>  Wed, 17 Jul 2019 00:52:33 +0000

faudio (19.06.07-3) unstable; urgency=medium

  * Fix autopkgtest regression introduced by the previous upload.

 -- Michael Gilbert <mgilbert@debian.org>  Fri, 12 Jul 2019 00:57:39 +0000

faudio (19.06.07-2) unstable; urgency=medium

  * Update standards version to 4.4.0.
  * Install tests to multiarch directories (closes: #930466).

 -- Michael Gilbert <mgilbert@debian.org>  Sun, 09 Jun 2019 04:35:04 +0000

faudio (19.06.07-1) unstable; urgency=medium

  * New upstream release.
  * Enable support for the xWMA codec.
  * Build tests and add them to autopkgtest.
  * Fix name of the shared object (closes: #927451).
  * Add cmake to the build dependencies (closes: #926825).
  * List files from Sean Barrett in debian/copyright (closes: #926578).

 -- Michael Gilbert <mgilbert@debian.org>  Sun, 09 Jun 2019 00:49:52 +0000

faudio (19.02-1) unstable; urgency=medium

  * Initial release.

 -- Michael Gilbert <mgilbert@debian.org>  Tue, 26 Feb 2019 20:53:57 -0500
